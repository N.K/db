import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class sql {


	public static void main(String[] args) {

		Connection c = null;
		PreparedStatement prepared_stmt= null;
		ResultSet rs = null;

		String sql = "";
		String ans;
		
		boolean exit = false;

		Scanner in = new Scanner(System.in);

		do
		{
			System.out.println("Ποιο ερώτημα θες να εκτελέσεις; (exit για έξοδο)");

			System.out.println("a. Ποια είναι τα μοντέλα αυτοκινήτων με το μέγιστο πλήθος ζημιών;");
			System.out.println("b. Ποιο είναι το μέσο κέρδος της εταιρείας από επισκευές (= μέσο κόστος ζημιών) ανά μήνα;");
			System.out.println("c. Ποιος είναι ο πωλητής με το μέγιστο «τζίρο» (= αξία πωλήσεων – αξία αγορών);");
			System.out.println("d. Ποιες είναι οι επισκευές που βρίσκονται σε εκκρεμότητα;");
			System.out.println("e. Ποιες είναι οι εργασίες του τεχνικού ‘X’ τον τελευταία μήνα;");
			System.out.println("f. Ποια είναι τα αυτοκίνητα που έχουν έρθει για επισκευή πάνω από 1 φορά τον τελευταίο χρόνο;");
			
			ans = in.nextLine();

			switch (ans) {

			case "a":

				sql =	"WITH sum_repairs AS (\r\n" +
							"SELECT c.model, COUNT(*) AS repairs\r\n" +
							"FROM repairs_history AS r INNER JOIN Cars AS c ON r.id_c = c.id\r\n" +
							"GROUP BY c.model\r\n" +
						")\r\n" +
						"SELECT *\r\n" +
						"FROM sum_repairs\r\n" +
						"WHERE repairs IN (SELECT MAX(repairs) FROM sum_repairs);";
				break;

			case "b":

				sql =	"SELECT AVG(est_cost) AS monthly_avg,TO_CHAR(DATE_TRUNC('month',start_date), 'YYYY-MM') AS per_month\r\n" + 
						"FROM repairs_history\r\n" + 
						"GROUP BY per_month\r\n" + 
						"ORDER BY per_month;";
				break;
			case "c":
				
				sql = "SELECT *\r\n" + 
						"FROM\r\n" + 
						"(\r\n" + 
						"	SELECT COALESCE(sells.id_s, buys.id_s) AS id_s, COALESCE(sell_sum, 0) - COALESCE(buy_sum, 0) AS profit\r\n" + 
						"	FROM\r\n" + 
						"	(\r\n" + 
						"		SELECT id_s, s.type, SUM(s.value) AS sell_sum\r\n" + 
						"		FROM sell_buy_history AS s\r\n" + 
						"		GROUP BY id_s, s.type\r\n" + 
						"		HAVING s.type='Sell'\r\n" + 
						"	) sells\r\n" + 
						"	FULL JOIN\r\n" + 
						"	(\r\n" + 
						"		SELECT id_s, s.type, SUM(s.value) AS buy_sum\r\n" + 
						"		FROM sell_buy_history AS s\r\n" + 
						"		GROUP BY id_s, s.type\r\n" + 
						"		HAVING s.type='Buy'\r\n" + 
						"	) buys\r\n" + 
						"	ON sells.id_s = buys.id_s\r\n" + 
						"	ORDER BY profit DESC\r\n" + 
						"	LIMIT 1\r\n" + 
						") max_profit\r\n" + 
						"INNER JOIN\r\n" + 
						"employees as e\r\n" + 
						"ON max_profit.id_s = e.id;";
				break;
			case "d":
				
				sql =	"SELECT id_t as technician, ID_c as car, manufacturer, model, est_cost, start_date\r\n" + 
						"FROM repairs_history, cars\r\n" + 
						"WHERE finish_date IS null AND ID_c = cars.ID;";
				break;
			case "e":
				
				sql =	"SELECT r.id_c as car, r.start_date, r.est_cost, c.model, c.manufacturer\r\n" + 
						"FROM employees AS e INNER JOIN repairs_history AS r\r\n" + 
						"ON e.ID = r.ID_t\r\n" + 
						"INNER JOIN cars AS c\r\n" + 
						"ON c.ID = r.ID_c\r\n" + 
						"WHERE r.start_date > current_date - interval '1 month' AND e.id = ?;";
				break;
			case "f":
				
				sql =	"SELECT c.id AS car, c.model, c.manufacturer, c.making_date\r\n" + 
						"FROM\r\n" + 
						"(\r\n" + 
						"	SELECT *\r\n" + 
						"	FROM repairs_history\r\n" + 
						"	WHERE start_date >= current_date - interval '1 year'\r\n" + 
						") cars_last_year\r\n" + 
						"INNER JOIN\r\n" + 
						"cars AS c\r\n" + 
						"ON cars_last_year.id_c = c.id\r\n" + 
						"GROUP BY c.ID\r\n" + 
						"HAVING COUNT(ID_c) > 1\r\n" + 
						"ORDER BY c.id;";
				break;
			case "exit":
				exit = true;
				break;
			default:
				System.out.println("Λάθος επιλογή");
				continue;
			}
			
			if (exit) break;

			try {

				c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Ergasia_Baseis",
						"postgres", "admin");

				System.out.println("Opened database successfully");

				prepared_stmt = c.prepareStatement(sql);

				if (ans.equals("e"))
				{
					while (true)
					{
						System.out.println("Για ποιον τεχνικό θες να ψάξω;");
						String t = in.nextLine();
						int i;
						try {
							i = Integer.parseInt(t);
							prepared_stmt.setInt(1, i);
							break;
						} catch (NumberFormatException e)
						{
							System.out.println("Δώσε το id του τεχνικού.");
						};
					}
				}

				rs = prepared_stmt.executeQuery();

				List<String> out = new ArrayList<String>();

				while( rs.next() )
				{
					for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++)
					{
						int type = rs.getMetaData().getColumnType(i);
						
						switch (type) {
						case -5:
							long bigint = rs.getLong(i);
							out.add(Long.toString(bigint));
							break;
						case 1:
							String ch = rs.getString(i);
							out.add(ch);
							break;
						case 4:
							int integer = rs.getInt(i);
							out.add(Integer.toString(integer));
							break;
						case 7:
							float fl = rs.getFloat(i);
							out.add(Float.toString(fl));
							break;
						case 8:
							double d = rs.getDouble(i);
							out.add(Double.toString(d));
							break;
						case 12:
							String text = rs.getString(i);
							out.add(text);
							break;
						case 91:
							Date date = rs.getDate(i);
							out.add(date.toString());
							break;
						case 93:
							Timestamp timestamp = rs.getTimestamp(i);
							out.add(timestamp.toString());
							break;
						default:
							break;
						}
					}
					
					System.out.println(String.join(", ", out));
					
					System.out.println();
					out.clear();
				}

			} catch (SQLException ex) {
				Logger.getLogger(sql.class.getName()).log(Level.SEVERE, null, ex);
			} finally {
				try {
					if (rs != null) {
						rs.close();
					}
					if (prepared_stmt != null) {
						prepared_stmt.close();
					}
					if (c != null) {
						c.close();
					}
				} catch (SQLException ex) {
					Logger.getLogger(sql.class.getName()).log(Level.SEVERE, null, ex);
				}
			}

		} while(ans != "exit");
		
		in.close();
	}
}