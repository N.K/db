CREATE TABLE Employees (
	ID serial NOT NULL,
	AFM char(9) NOT NULL UNIQUE,
	name TEXT NOT NULL,
	surname TEXT NOT NULL,
	email TEXT NOT NULL,
	job TEXT NOT NULL,
	CONSTRAINT Employees_pk PRIMARY KEY (ID)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Repairs_History (
	ID_t bigint NOT NULL,
	ID_c bigint NOT NULL,
	start_date DATE NOT NULL,
	est_cost real NOT NULL,
	finish_date DATE,
	CONSTRAINT Repairs_History_pk PRIMARY KEY (ID_t,ID_c,start_date)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Sell_Buy_History (
	ID_c bigint NOT NULL,
	type TEXT NOT NULL,
	purchase_date DATE NOT NULL,
	ID_s bigint NOT NULL,
	value real NOT NULL,
	CONSTRAINT Sell_Buy_History_pk PRIMARY KEY (ID_c,type,purchase_date)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Cars (
	ID serial NOT NULL,
	AFM_owner char(9),
	number_traffic char(8) UNIQUE,
	manufacturer TEXT,
	model TEXT,
	making_date DATE,
	state TEXT,
	CONSTRAINT Cars_pk PRIMARY KEY (ID)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Customers (
	AFM char(9) NOT NULL,
	name TEXT NOT NULL,
	surname TEXT NOT NULL,
	telephone char(10) NOT NULL,
	email TEXT NOT NULL,
	CONSTRAINT Customers_pk PRIMARY KEY (AFM)
) WITH (
  OIDS=FALSE
);




ALTER TABLE Repairs_History ADD CONSTRAINT Repairs_History_fk0 FOREIGN KEY (ID_t) REFERENCES Employees(ID);
ALTER TABLE Repairs_History ADD CONSTRAINT Repairs_History_fk1 FOREIGN KEY (ID_c) REFERENCES Cars(ID);

ALTER TABLE Sell_Buy_History ADD CONSTRAINT Sell_Buy_History_fk0 FOREIGN KEY (ID_c) REFERENCES Cars(ID);
ALTER TABLE Sell_Buy_History ADD CONSTRAINT Sell_Buy_History_fk1 FOREIGN KEY (ID_s) REFERENCES Employees(ID);

ALTER TABLE Cars ADD CONSTRAINT Cars_fk0 FOREIGN KEY (AFM_owner) REFERENCES Customers(AFM);


