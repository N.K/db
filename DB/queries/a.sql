WITH sum_repairs AS (
	SELECT c.model, COUNT(*) AS repairs
	FROM repairs_history AS r INNER JOIN Cars AS c ON r.id_c = c.id
	GROUP BY c.model
)
SELECT *
FROM sum_repairs
WHERE repairs IN (SELECT MAX(repairs) FROM sum_repairs);
