SELECT c.id AS car, c.model, c.manufacturer, c.making_date
FROM
(
	SELECT *
	FROM repairs_history
	WHERE start_date >= current_date - interval '1 year'
) cars_last_year
INNER JOIN
cars AS c
ON cars_last_year.id_c = c.id
GROUP BY c.ID
HAVING COUNT(ID_c) > 1
ORDER BY c.id
