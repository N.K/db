SELECT r.id_t as technician, r.ID_c as car, c.manufacturer, c.model, r.est_cost, r.start_date
FROM repairs_history as r, cars as c
WHERE finish_date IS null AND ID_c = c.ID;
