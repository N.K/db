SELECT AVG(est_cost) AS monthly_avg,TO_CHAR(DATE_TRUNC('month',start_date), 'YYYY-MM') AS per_month
FROM repairs_history
GROUP BY per_month
ORDER BY per_month;
