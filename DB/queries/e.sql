SELECT r.id_c as car, r.start_date, r.est_cost, c.model, c.manufacturer
FROM employees AS e INNER JOIN repairs_history AS r
ON e.ID = r.ID_t
INNER JOIN cars AS c
ON c.ID = r.ID_c
WHERE r.start_date > current_date - interval '1 month' AND e.id = '169';
