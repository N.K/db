SELECT *
FROM
(
	SELECT COALESCE(sells.id_s, buys.id_s) AS id_s, COALESCE(sell_sum, 0) - COALESCE(buy_sum, 0) AS profit
	FROM
	(
		SELECT id_s, s.type, SUM(s.value) AS sell_sum
		FROM sell_buy_history AS s
		GROUP BY id_s, s.type
		HAVING s.type='Sell'
	) sells
	FULL JOIN
	(
		SELECT id_s, s.type, SUM(s.value) AS buy_sum
		FROM sell_buy_history AS s
		GROUP BY id_s, s.type
		HAVING s.type='Buy'
	) buys
	ON sells.id_s = buys.id_s
	ORDER BY profit DESC
	LIMIT 1
) max_profit
INNER JOIN
employees as e
ON max_profit.id_s = e.id;
