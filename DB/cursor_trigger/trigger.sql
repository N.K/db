CREATE TRIGGER car_repair BEFORE INSERT
ON repairs_history
FOR EACH ROW
EXECUTE PROCEDURE auto_insert_car()
