CREATE OR REPLACE FUNCTION query_with_cursor()
RETURNS TABLE (id_c bigint, manufacturer text, model text, start_date date)
AS $$
DECLARE
	cur_d refcursor;
	tmp_rec RECORD;
BEGIN
	OPEN cur_d SCROLL FOR
	SELECT r.id_t as technician, r.ID_c as car, c.manufacturer, c.model, r.est_cost, r.start_date
	FROM repairs_history as r, cars as c
	WHERE finish_date IS null AND r.ID_c = c.ID;

	LOOP
		FETCH FROM cur_d INTO tmp_rec;
		EXIT WHEN NOT FOUND;
			id_c := tmp_rec.car;
			manufacturer := tmp_rec.manufacturer;
			model := tmp_rec.model;
			start_date := tmp_rec.start_date;
		RETURN NEXT;
	END LOOP;
	CLOSE cur_d;
END;
$$ LANGUAGE plpgsql;

