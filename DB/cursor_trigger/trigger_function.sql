CREATE OR REPLACE FUNCTION auto_insert_car() RETURNS TRIGGER
AS $$
BEGIN
	IF NEW.id_c not in (SELECT c.ID from cars as c) THEN
	INSERT INTO cars(ID)
	VALUES (NEW.id_c);
	END IF;
	
	RETURN NEW;
END;
$$ LANGUAGE plpgsql

